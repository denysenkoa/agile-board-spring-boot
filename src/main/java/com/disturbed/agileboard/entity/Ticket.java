package com.disturbed.agileboard.entity;

public class Ticket {

    private String title;
    private String description;
    private String state;

    public Ticket(String title, String description, String state) {
        this.title = title;
        this.description = description;
        this.state = state;
    }

    public Ticket() {
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getState() {
        return state;
    }
}

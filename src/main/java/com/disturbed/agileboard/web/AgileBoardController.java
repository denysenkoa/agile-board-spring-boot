package com.disturbed.agileboard.web;

import com.disturbed.agileboard.db.DataBase;
import com.disturbed.agileboard.entity.Ticket;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AgileBoardController {

    @CrossOrigin
    @RequestMapping(value = "/tickets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<Ticket>> getAllTickets() {
        try {
            return new ResponseEntity<>(DataBase.tikets, HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @CrossOrigin
    @RequestMapping(value = "/tickets", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<Ticket>> addTicket(@RequestBody Ticket ticket) {
        try {
            DataBase.tikets.add(ticket);
            return new ResponseEntity<>(DataBase.tikets, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/tickets/{ticketId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<Ticket>> updateTicket(@PathVariable("ticketId") String ticketId, @RequestBody Ticket ticket) {
        try {
            DataBase.tikets.removeIf(t -> t.getTitle().equals(ticketId));
            DataBase.tikets.add(ticket);

            return new ResponseEntity<>(DataBase.tikets, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/tickets/{ticketId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<Ticket>> removeTicket(@PathVariable("ticketId") String ticketId) {
        try {
            System.out.println("Ticket id = " + ticketId);
            DataBase.tikets.removeIf(t -> t.getTitle().equals(ticketId));
            return new ResponseEntity<>(DataBase.tikets, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

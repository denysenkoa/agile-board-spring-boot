package com.disturbed.agileboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgileBoardBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AgileBoardBackendApplication.class, args);
    }
}
